
let firstNumber;
let secondNumber;
let operation;
let first;
let second;

do {
  first = prompt('Please enter first number', first);
  second = prompt(('Please enter second number'), second);
  operation = prompt('Enter operation(+, -, /, *)', operation);
  firstNumber = Number(first);
  secondNumber = Number(second);

} while (isInvalid(firstNumber, secondNumber, operation));

function isInvalid(firstNumber, secondNumber, operation) {
  if (firstNumber === null || secondNumber === null || !operation) {
    return true;
  }
  if (isNaN(firstNumber) || isNaN(secondNumber)) {
    return true;
  }
  if (operation !== "+" && operation !== "-" && operation !== "/" && operation !== "*") {
    return true;
  }

  return false;
}

console.log(calculate(firstNumber, secondNumber, operation));

function add(x, y) {
  return x + y;
}

function subtraction(x, y) {
  return x - y;
}

function division(x, y) {
  return x / y;
}

function multiplication(x, y) {
  return x * y;
}

function calculate(a, b, operation) {
  let result;

  // if (operation === '+') {
  //   result = add(a, b);
  // } else if (operation === '-') {
  //   result = subtraction(a, b);
  // } else if (operation === '/') {
  //   result = division(a, b);
  // } else if (operation === '*') {
  //   result = multiplication(a, b);
  // }

  switch (operation) {
    case '+':
      result = add(a, b)
      break;
    case '-':
      result = subtraction(a, b)
      break;
    case '/':
      result = division(a, b)
      break;
    case '*':
      result = multiplication(a, b)
      break;

    default:
      break;
  }
  return result;
}

